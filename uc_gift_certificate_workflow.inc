<?php
/*
 * Implementation of hook_action_info().
 */
function uc_gift_certificate_action_info() {
  $order_arg = array('#entity' => 'order', '#label' => t('Order'));
  $actions['uc_gift_certificate_action_addcerts'] = array(
    '#label' => t('Add Gift Certificate values to the user'),
    '#module' => t('Gift Certificates'),
    '#description' => t('Adds gift certificate values to the user'),
    '#arguments' => array('order' => $order_arg),
  );
  return $actions;
}

/*
 * Action callback. Send the user some certificates.
 */
function uc_gift_certificate_action_addcerts($order, $settings, &$arguments, &$log) {
  $amount = $settings['amount'];
  $user = user_load(array('uid' => $order->uid));
  $gift = uc_gift_certificates_gift($user, $settings['amount']);
  db_query("UPDATE {uc_gift_certificates} SET order_id = %d WHERE certificate_id = %d", $order->order_id, $gift['cert_id']);
  uc_order_comment_save($order->order_id, $user->uid, t('Added gift certificate !code worth !value.',
    array('!code' => $gift['cert_code'], '!value' => uc_currency_format($settings['amount']))), 'admin');
}

/*
 * Action user: add user roles form
 */
function uc_gift_certificate_action_addcerts_form($settings = array(), $argument_info) {
  $form = array();
  $form['amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Amount'),
    '#default_value' => $settings['amount'],
    '#required' => TRUE,
  );
  return $form;
}

function uc_gift_certificate_action_addcerts_submit($form_id, $form_values) {
  $settings = array('amount' => $form_values['amount']);
  return $settings;
}